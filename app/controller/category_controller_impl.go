package controller

import (
	web2 "belajar-golang-restful-api/app/entity/web"
	"belajar-golang-restful-api/app/services"
	helper2 "belajar-golang-restful-api/pkg/helper"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

type CategoryControllerImpl struct {
	CategoryService services.CategoryService
}

func NewCategoryController(categoryService services.CategoryService) CategoryController {
	return &CategoryControllerImpl{
		CategoryService: categoryService,
	}
}

func (controller *CategoryControllerImpl) Create(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryCreateRequest := web2.CategoryCreateRequest{}
	helper2.ReadFromRequestBody(request, &categoryCreateRequest)

	categoryResponse := controller.CategoryService.Create(request.Context(), categoryCreateRequest)
	webResponse := web2.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   categoryResponse,
	}

	helper2.WriteToResponseBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) Update(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryUpdateRequest := web2.CategoryUpdateRequest{}
	helper2.ReadFromRequestBody(request, &categoryUpdateRequest)

	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper2.PanicIfError(err)

	categoryUpdateRequest.Id = id

	categoryResponse := controller.CategoryService.Update(request.Context(), categoryUpdateRequest)
	webResponse := web2.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   categoryResponse,
	}

	helper2.WriteToResponseBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) Delete(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper2.PanicIfError(err)

	controller.CategoryService.Delete(request.Context(), id)
	webResponse := web2.WebResponse{
		Code:   200,
		Status: "OK",
	}

	helper2.WriteToResponseBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) FindById(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryId := params.ByName("categoryId")
	id, err := strconv.Atoi(categoryId)
	helper2.PanicIfError(err)

	categoryResponse := controller.CategoryService.FindById(request.Context(), id)
	webResponse := web2.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   categoryResponse,
	}

	helper2.WriteToResponseBody(writer, webResponse)
}

func (controller *CategoryControllerImpl) FindAll(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	categoryResponses := controller.CategoryService.FindAll(request.Context())
	webResponse := web2.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   categoryResponses,
	}

	helper2.WriteToResponseBody(writer, webResponse)
}
