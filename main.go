package main

import (
	"belajar-golang-restful-api/app/controller"
	"belajar-golang-restful-api/app/controller/middleware"
	"belajar-golang-restful-api/app/repository/category"
	"belajar-golang-restful-api/app/services"
	"belajar-golang-restful-api/pkg/database"
	"belajar-golang-restful-api/pkg/helper"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
)

func main() {

	db := database.NewDB()
	validate := validator.New()
	categoryRepository := category.NewCategoryRepository()
	categoryService := services.NewCategoryService(categoryRepository, db, validate)
	categoryController := controller.NewCategoryController(categoryService)
	router := controller.NewRouter(categoryController)

	server := http.Server{
		Addr:    "localhost:3000",
		Handler: middleware.NewAuthMiddleware(router),
	}

	err := server.ListenAndServe()
	helper.PanicIfError(err)
}
